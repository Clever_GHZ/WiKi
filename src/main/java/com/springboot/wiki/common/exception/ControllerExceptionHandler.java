package com.springboot.wiki.common.exception;

import com.springboot.wiki.common.CommonResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 可以对controller的异常进行一些统一异常处理，是一个异常处理类
 */
@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {
    /**
     * 校验异常统一处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = BindException.class)
    @ResponseBody
    public CommonResp validExceptionHandler(BindException e) {
        CommonResp commonResp = new CommonResp();
        log.warn("参数校验失败：{}", e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        commonResp.setStatus(500);
        commonResp.setMsg(e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        return commonResp;
    }

    /**
     * 处理BusinessException的异常
     *
     * @param
     * @return
     */
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public CommonResp validExceptionHandler(BusinessException e) {
        CommonResp commonResp = new CommonResp();
        log.warn("业务异常：{}", e.getCode().getDesc());
        commonResp.setStatus(500);
        commonResp.setMsg(e.getCode().getDesc());
        return commonResp;
    }

    /**
     * 处理系统异常，处理大路货色的异常
     *
     * @param
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public CommonResp validExceptionHandler(Exception e) {
        CommonResp commonResp = new CommonResp();
        log.error("系统异常：", e);
        commonResp.setStatus(500);
        commonResp.setMsg("系统出现异常");
        return commonResp;
    }

}
