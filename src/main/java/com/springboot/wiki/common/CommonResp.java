package com.springboot.wiki.common;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * @Title: JSONResult.java
 * @Package com.imooc.utils
 * @Description: 自定义响应数据结构
 * <p>
 * 200：表示成功
 * 500：表示错误，错误信息在message字段中
 * 501：bean验证错误，不管多少个错误都以map形式返回
 * 502：拦截器拦截到用户token出错
 * 555：异常抛出信息
 */

public class CommonResp<T> {


    // 响应业务状态
    private Integer status;

    // 响应消息
    private String message;

    // 响应中的数据
    private Object data;

    @JsonIgnore
    private String ok;

    public static CommonResp build(Integer status, String msg, Object data) {
        return new CommonResp(status, msg, data);
    }

    public static CommonResp build(Integer status, String msg, Object data, String ok) {
        return new CommonResp(status, msg, data, ok);
    }

    public static CommonResp ok(Object data) {
        return new CommonResp(data);
    }

    public static CommonResp ok(Object data, String msg) {
        return new CommonResp(data, msg);
    }

    public static CommonResp ok(Integer status, String message, Object data) {
        return new CommonResp(status, message, data);
    }

    public static CommonResp ok() {
        return new CommonResp(null);
    }

    public static CommonResp errorMsg(String msg) {
        return new CommonResp(500, msg, null);
    }

    public static CommonResp errorMsg(String msg, String data) {
        return new CommonResp(500, msg, data);
    }

    public static CommonResp errorMsgs(String msg, String data) {
        return new CommonResp(99, msg, data);
    }

    public static CommonResp errorMsgs(String msg) {
        return new CommonResp(99, msg);
    }

    public static CommonResp errorMap(Object data) {
        return new CommonResp(501, "error", data);
    }

    public static CommonResp errorTokenMsg(String msg) {
        return new CommonResp(502, msg, null);
    }

    public static CommonResp errorException(String msg) {
        return new CommonResp(555, msg, null);
    }

    public static CommonResp errorUserQQ(String msg) {
        return new CommonResp(556, msg, null);
    }

    public CommonResp() {
        this.status = 200;
    }

    public CommonResp(Integer status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public CommonResp(Integer status, String msg, Object data, String ok) {
        this.status = status;
        this.message = msg;
        this.data = data;
        this.ok = ok;
    }

    public CommonResp(Object data) {
        this.status = 200;
        this.message = "OK";
        this.data = data;
    }

    public CommonResp(Object data, String msg) {
        this.status = 200;
        this.message = msg;
        this.data = data;
    }


    public Boolean isOK() {
        return this.status == 200;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

}
