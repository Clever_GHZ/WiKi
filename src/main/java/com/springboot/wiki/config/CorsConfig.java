package com.springboot.wiki.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 解决前后端跨域问题
 */

@Configuration
public class CorsConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")//针对所有的接口
                .allowedOriginPatterns("*")//允许来源
                .allowedHeaders(CorsConfiguration.ALL)//所有的
                .allowedMethods(CorsConfiguration.ALL)//方法都支持
                .allowCredentials(true)//带上凭证，cookies什么的
                .maxAge(3600); // 1小时内不需要再预检（发OPTIONS请求）
    }

}