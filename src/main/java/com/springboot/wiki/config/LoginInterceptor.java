package com.springboot.wiki.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 拦截器：Spring框架特有的，常用于登录校验，权限校验，请求日志打印
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginInterceptor.class);
    @Resource
    private RedisTemplate redisTemplate; // 注入redis

    /**
     * 登录校验
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 打印信息
        LOGGER.info("--------------LoginInterceptor开始执行------------");
        long startTime = System.currentTimeMillis(); // 开始时间
        request.setAttribute("requestStartTime", startTime);
        // OPTIONS请求不做校验,
        // 前后端分离的架构, 前端会发一个OPTIONS请求先做预检, 对预检请求不做校验
        if (request.getMethod().toUpperCase().equals("OPTIONS")) {
            return true;
        }
        String path = request.getRequestURL().toString(); // 请求路径
        LOGGER.info("接口登录拦截：path", path);
        // 获取header中的token
        String token = request.getHeader("token");
        LOGGER.info("登录校验开始，token：{}", token);
        if (token == null || token.isEmpty()) {
            LOGGER.info("token为空,请求被拦截");
            response.setStatus(HttpStatus.UNAUTHORIZED.value()); // 返回未授权的
            return false;
        }
        // 查看redis中的token
        Object token1 = redisTemplate.opsForValue().get("token");
        if (token1 == null) {
            LOGGER.info("token无效,请求被拦截");
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return false;
        } else {
            LOGGER.info("已登录：{}", token1);
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        long startTime = (Long) request.getAttribute("requestStartTime");
        LOGGER.info("------------- LoginInterceptor 结束 耗时：{} ms -------------", System.currentTimeMillis());
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
