package com.springboot.wiki.mapper;

import com.springboot.wiki.domain.Content;
import com.springboot.wiki.domain.Doc;
import com.springboot.wiki.domain.Example.DocExample;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DocMapper {
    long countByExample(DocExample example);

    int deleteByExample(DocExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Doc record);

    int insertSelective(Doc record);

    List<Doc> selectByExample(DocExample example);

    Doc selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record")Doc record, @Param("example") Doc example);

    int updateByExample(@Param("record") Doc record, @Param("example") Doc example);

    int updateByPrimaryKeySelective(Doc record);

    int updateByPrimaryKey(Doc record);
}
