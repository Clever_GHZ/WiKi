package com.springboot.wiki.mapper;

import com.springboot.wiki.domain.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: guohuaizhao
 * @Date: 2021/10/09/17:46
 * @Description:
 */
public interface TestMapper {
    //方法中返回一个List List中有一个Test类。方法名叫list()，暂时不要参数。
    public List<Test> list();
}
