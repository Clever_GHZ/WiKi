package com.springboot.wiki.controller;

import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.dto.EbookQueryDTO;
import com.springboot.wiki.dto.EbookSaveDTO;
import com.springboot.wiki.resp.EbookQueryResp;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.service.EbookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * Created with IntelliJ IDEA.
 *
 * @Author: guohuaizhao
 * @Date: 2021/09/29/13:48
 * @Description: controller接口层
 */
@Api(value = "电子书相关", tags = "电子书相关")
@RestController
@RequestMapping("/ebook")
public class EbookController {
    private static final Logger LOG = LoggerFactory.getLogger(EbookController.class);
    //定义私有的方法
    @Resource
    private EbookService ebookService;

    @ApiOperation(value = "查询电子书", notes = "", httpMethod = "GET")
    @GetMapping("/list")
    public CommonResp list(@Valid EbookQueryDTO ebookQueryDTO) {
        CommonResp<PageResp<EbookQueryResp>> resp = new CommonResp<>();
        PageResp<EbookQueryResp> list = ebookService.list(ebookQueryDTO);
        resp.setData(list);
        resp.setMsg("查询成功");
        return resp;
    }

    @ApiOperation(value = "新增操作或更新电子书", notes = "", httpMethod = "POST")
    @PostMapping("/save")
    public CommonResp save(@Valid @RequestBody EbookSaveDTO ebookSaveDTO) {
        return ebookService.save(ebookSaveDTO);
    }

    @ApiOperation(value = "单个删除或者批量删除电子书", notes = "", httpMethod = "POST")
    @PostMapping("/delete")
    public CommonResp delete(@PathVariable String ids) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + ids);
        return ebookService.delete(ids);
    }


}
