package com.springboot.wiki.controller;

import com.springboot.wiki.domain.Test;
import com.springboot.wiki.service.TestService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 *
 * @Author: guohuaizhao
 * @Date: 2021/09/29/13:48
 * @Description: controller接口层
 */
@Api(value = "test", tags = "test测试")

@RestController
public class TestController {
    private static final Logger LOG = LoggerFactory.getLogger(TestController.class);
    @Value("${test.hello}")
    private String testHello;
    //将service层的testservice引入
    @Resource//将service注入进来
    private TestService testService;

    @GetMapping("/hello")
    public String hello() {
        return "hello word!" + testHello;
    }

    @PostMapping("/hello1")
    public String hello1() {
        return "hello word!!!";
    }

    //"/test/list"为请求接口路径
    @GetMapping("/test/list")
    //List<Test>与service层的一致
    public List<Test> lest() {
        //返回testService并调用其中的list()方法
        return testService.list();
    }
}
