package com.springboot.wiki.controller;

import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.dto.CategoryQueryDTO;
import com.springboot.wiki.dto.CategorySaveDTO;
import com.springboot.wiki.resp.CategoryQueryResp;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 类别控制器
 */
@Api(value = "电子书类别相关", tags = "电子书类别相关")
@RestController
@RequestMapping("/category")
public class CategoryController {
    // 依赖注入
    @Resource
    private CategoryService categoryService;

    @ApiOperation(value = "查询电子书类别", notes = "", httpMethod = "GET")
    @GetMapping("/list")
    public CommonResp list(@Valid CategoryQueryDTO categoryQueryDTO) {
        // 创建实例接受对象
        CommonResp<PageResp<CategoryQueryResp>> resp = new CommonResp<>();
        PageResp<CategoryQueryResp> list = categoryService.list(categoryQueryDTO);
        resp.setData(list);
        return CommonResp.ok(resp);
    }

    @ApiOperation(value = "查询全部电子书类别", notes = "", httpMethod = "GET")
    @GetMapping("/all")
    public CommonResp all() {
        return categoryService.all();
    }

    @ApiOperation(value = "保存电子书类别", notes = "", httpMethod = "POST")
    @PostMapping("/save")
    public CommonResp save(@Valid @RequestBody CategorySaveDTO categorySaveDTO) {
        return categoryService.save(categorySaveDTO);
    }

    @ApiOperation(value = "删除电子书类别", notes = "", httpMethod = "DELETE")
    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id) {
        return categoryService.delete(id);
    }


}
