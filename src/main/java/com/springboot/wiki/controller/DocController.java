package com.springboot.wiki.controller;

import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.dto.DocQueryDTO;
import com.springboot.wiki.dto.DocSaveDTO;
import com.springboot.wiki.service.DocService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(value = "文档相关", tags = "文档相关")
@RestController
@RequestMapping("/doc")
public class DocController {
    @Resource
    private DocService docService;

    @ApiOperation(value = "查询文档信息")
    @GetMapping("/list")
    public CommonResp list(@Valid DocQueryDTO docQueryDTO) {
        return docService.list(docQueryDTO);
    }

    @ApiOperation(value = "查询所有文档信息")
    @GetMapping("/all/{ebookId}")
    public CommonResp all(@PathVariable Long ebookId) {
        return docService.all(ebookId);
    }

    @ApiOperation(value = "新增文档信息")
    @PostMapping("/save")
    public CommonResp save(@Valid @RequestBody DocSaveDTO docSaveDTO) {
        return docService.save(docSaveDTO);
    }

    @ApiOperation(value = "单个删除或批量删除文档信息")
    @PostMapping("/delete")
    public CommonResp delete(@RequestBody String idsStr) {
        return docService.delete(idsStr);
    }

    @ApiOperation(value = "查找内容")
    @GetMapping("/find-content/{id}")
    public CommonResp findContent(@PathVariable Long id) {
        return docService.findContent(id);
    }

    @ApiOperation(value = "投票")
    @GetMapping("/vote/{id}")
    public CommonResp vote(@PathVariable Long id) {
        return docService.vote(id);
    }

}
