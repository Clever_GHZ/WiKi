package com.springboot.wiki.controller;

import com.alibaba.fastjson.JSONObject;
import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.dto.UserLoginDTO;
import com.springboot.wiki.dto.UserQueryDTO;
import com.springboot.wiki.dto.UserResetPasswordDTO;
import com.springboot.wiki.dto.UserSaveDTO;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.resp.UserQueryResp;
import com.springboot.wiki.service.UserService;
import com.springboot.wiki.utils.SnowFlakeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

@Api(value = "用户相关", tags = "用户相关接口")
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Resource
    private UserService userService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private SnowFlakeUtil snowFlake;


    @ApiOperation(value = "查询")
    @GetMapping("/list")
    public CommonResp list(@Valid UserQueryDTO userQueryDTO) {
        return userService.list(userQueryDTO);
    }

    @ApiOperation(value = "用户注册")
    @PostMapping("/save")
    public CommonResp save(@Valid @RequestBody UserSaveDTO UserSaveDTO) {
        return userService.save(UserSaveDTO);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + id);
        return userService.delete(id);
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("/reset-password")
    public CommonResp resetPassword(@Valid @RequestBody UserResetPasswordDTO userResetPasswordDTO) {
        return userService.resetPassword(userResetPasswordDTO);
    }

    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    public CommonResp login(@Valid @RequestBody UserLoginDTO userLoginDTO) {
        return userService.login(userLoginDTO);
    }

    @GetMapping("/logout/{token}")
    public CommonResp logout(@PathVariable String token) {
        Boolean delete = redisTemplate.delete(token);
        log.info("从redis中删除token: {}", token);
        if (delete) {
            return CommonResp.ok("退出成功");
        } else {
            return CommonResp.errorMsg("退出失败");
        }
    }

}
