package com.springboot.wiki.controller;

import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.service.EbookSnapshotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(value = "电子书快照模块", tags = "电子书快照接口")
@RestController
@RequestMapping("/ebook-snapshot")
@Slf4j
public class EbookSnapshotController {

    @Resource
    private EbookSnapshotService ebookSnapshotService;


    @ApiOperation(value = "获取电子书快照")
    @GetMapping("/get-statistic")
    public CommonResp getStatistic() {
        log.info("开始调用接口数据");
        return ebookSnapshotService.getStatistic();
    }

    /**
     * 功能描述:调用三十天的增长接口
     */
    @ApiOperation(value = "调用三十天数据统计")
    @GetMapping("/get-30-statistic")
    public CommonResp get30Statistic() {
        return ebookSnapshotService.get30Statistic();
    }


}
