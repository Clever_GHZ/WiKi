package com.springboot.wiki.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EbookQueryDTO extends PageDTO {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String name;

    private Long categoryId2;
}