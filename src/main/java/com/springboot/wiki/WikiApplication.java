package com.springboot.wiki;


import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableAsync
//包扫描 支持多包扫描
@ComponentScan("com.springboot")
@SpringBootApplication
//扫描mapper层让项目知道mapper文件夹下的是持久层
@MapperScan("com.springboot.wiki.mapper")
//config 配置包
public class WikiApplication {

    //打印日志
    private static final Logger LOG = LoggerFactory.getLogger(WikiApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(WikiApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功");
        LOG.info("地址：\thttp:127.0.0.1:{}", env.getProperty("server.port"));
    }

}

