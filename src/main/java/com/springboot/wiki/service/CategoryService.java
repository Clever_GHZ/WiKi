package com.springboot.wiki.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.domain.Category;

import com.springboot.wiki.domain.Example.CategoryExample;
import com.springboot.wiki.dto.CategoryQueryDTO;
import com.springboot.wiki.dto.CategorySaveDTO;
import com.springboot.wiki.mapper.CategoryMapper;
import com.springboot.wiki.resp.CategoryQueryResp;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.utils.CopyUtil;
import com.springboot.wiki.utils.SnowFlakeUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);
    @Resource
    private CategoryMapper categoryMapper;
    // 引入雪花依赖
    @Resource
    private SnowFlakeUtil snowFlake;

    /**
     * 分页查询
     */
    public PageResp<CategoryQueryResp> list(CategoryQueryDTO categoryQueryDTO) {
        PageHelper.startPage(categoryQueryDTO.getPage(), categoryQueryDTO.getSize());

        // 使用构造器创建查询条件
        CategoryExample example = new CategoryExample();
        example.setOrderByClause("sort asc"); // 排序:升序
        List<Category> categoryList = categoryMapper.selectByExample(example);

        // 分页
        PageInfo<Category> categoryInfo = new PageInfo<>(categoryList);
        LOGGER.info("总行数:{}", categoryInfo.getTotal());
        LOGGER.info("总页数：{}", categoryInfo.getPages());

        // 将多条转换成可以返回的数据
        List<CategoryQueryResp> list = CopyUtil.copyList(categoryList, CategoryQueryResp.class);
        // 创建页面返回对象
        PageResp<CategoryQueryResp> pageResp = new PageResp<>();
        pageResp.setTotal(categoryInfo.getTotal());
        pageResp.setList(list);
        return pageResp; // 返回页面信息

    }

    /**
     * 查询全部
     */
    public CommonResp all() {
        CategoryExample example = new CategoryExample();
        example.setOrderByClause("sort asc");
        // 查到的所有字段
        List<Category> categoryList = categoryMapper.selectByExample(example);
        // 转换成前端需要得vo   将Category字段转换成CategoryQueryResp字段
        List<CategoryQueryResp> list = CopyUtil.copyList(categoryList, CategoryQueryResp.class);
        return CommonResp.ok(list);
    }

    /**
     * 保存类别
     */
    public CommonResp save(CategorySaveDTO categorySaveDTO) {
        if (null == categorySaveDTO) {
            return CommonResp.errorMsg("传入的数据参数为空");
        }
        LOGGER.info("传入类别DTO的数据，{}", categorySaveDTO);
        // 将传入的数据进行转换 参数1:前端传入的数据 参数2:实体类
        Category categoryData = CopyUtil.copy(categorySaveDTO, Category.class);
        // 存在更新 不存在添加
        if (ObjectUtils.isEmpty(categoryData.getId())) {
            categoryData.setId(snowFlake.nextId()); // 采用雪花算法
            categoryMapper.insert(categoryData); // 插入
        } else {
            categoryMapper.updateByPrimaryKey(categoryData);
        }
        return CommonResp.ok("保存成功");
    }

    /**
     * 删除类别
     */
    public CommonResp delete(Long id) {
        if (null == id) {
            return CommonResp.errorMsg("请选择需要删除的id");
        }
        LOGGER.info("删除的id,{}", id);
        categoryMapper.deleteByPrimaryKey(id);
        return CommonResp.ok("删除成功");
    }
}
