package com.springboot.wiki.service;

import com.springboot.wiki.mapper.TestMapper;
import com.springboot.wiki.domain.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: guohuaizhao
 * @Date: 2021/10/12/16:02
 * @Description: service服务层
 */
//使用Service注解将这个service交给Spring来管理。使spring扫描到这个类
@Service
public class TestService {
    //将mapper写进来 声明一个私有的方法
    @Resource//将testMapper注入进来 也可以使用@Resource注解
    private TestMapper testMapper;

    //写一个方法 返回list
    public List<Test> list() {
        //返回这个方法
        return testMapper.list();
    }

}
