package com.springboot.wiki.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.common.exception.BusinessException;
import com.springboot.wiki.common.exception.BusinessExceptionCode;
import com.springboot.wiki.domain.Example.UserExample;
import com.springboot.wiki.domain.User;
import com.springboot.wiki.dto.UserLoginDTO;
import com.springboot.wiki.dto.UserQueryDTO;
import com.springboot.wiki.dto.UserResetPasswordDTO;
import com.springboot.wiki.dto.UserSaveDTO;
import com.springboot.wiki.mapper.UserMapper;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.resp.UserLoginResp;
import com.springboot.wiki.resp.UserQueryResp;
import com.springboot.wiki.utils.CopyUtil;
import com.springboot.wiki.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class UserService {
    @Resource
    private UserMapper userMapper;


    @Resource
    private SnowFlakeUtil snowFlake;

    @Resource
    private RedisTemplate redisTemplate;


    /**
     * @param userQueryDTO
     * @return
     */
    public CommonResp list(UserQueryDTO userQueryDTO) {

        PageHelper.startPage(userQueryDTO.getPage(), userQueryDTO.getSize());
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();

        if (!ObjectUtils.isEmpty(userQueryDTO.getLoginName())) {
            criteria.andNameLike("%" + userQueryDTO.getLoginName() + "%");
        }
        List<User> userList = userMapper.selectByExample(userExample);
        PageInfo<User> pageInfo = new PageInfo<>(userList);
        log.info("总行数：{}", pageInfo.getTotal());
        log.info("总页数：{}", pageInfo.getPages());
//        List<UserResp> respList=new ArrayList<>();
//        for (User user : userList) {
//
//            UserResp userResp = CopyUtil.copy(user, UserResp.class);
//            respList.add(userResp);
//        }
        List<UserQueryResp> list = CopyUtil.copyList(userList, UserQueryResp.class);
        PageResp<UserQueryResp> pageResp = new PageResp<>();
        pageResp.setTotal(pageInfo.getTotal());
        pageResp.setList(list);
        return CommonResp.ok(pageResp);
    }

    /**
     * 保存信息
     */
    public CommonResp save(UserSaveDTO userSaveDTO) {
        userSaveDTO.setPassword(DigestUtils.md5DigestAsHex(userSaveDTO.getPassword().getBytes()));
        User user = CopyUtil.copy(userSaveDTO, User.class);
        if (ObjectUtils.isEmpty(user.getId())) {
            if (ObjectUtils.isEmpty(selectByLoginName(userSaveDTO.getLoginName()))) {
                user.setId(snowFlake.nextId());
                userMapper.insert(user);
            } else {
                //用户名已存在
                return CommonResp.errorMsg(String.valueOf(BusinessExceptionCode.USER_LOGIN_NAME_EXIST));
            }
        } else {
            user.setLoginName(null);
            user.setPassword(null);
            userMapper.updateByPrimaryKeySelective(user);
        }
        return CommonResp.ok("添加成功");
    }


    public User selectByLoginName(String LoginName) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andLoginNameEqualTo(LoginName);
        List<User> userList = userMapper.selectByExample(userExample);
        if (CollectionUtils.isEmpty(userList)) {
            return null;
        } else {
            return userList.get(0);
        }
    }


    /**
     * 删除
     */
    public CommonResp delete(Long id) {
        userMapper.deleteByPrimaryKey(id);
        return CommonResp.ok("删除成功");
    }

    /**
     * 更改密码
     */
    public CommonResp resetPassword(UserResetPasswordDTO userResetPasswordDTO) {
        userResetPasswordDTO.setPassword(DigestUtils.md5DigestAsHex(userResetPasswordDTO.getPassword().getBytes()));
        User user = CopyUtil.copy(userResetPasswordDTO, User.class);
        userMapper.updateByPrimaryKeySelective(user);
        return CommonResp.ok("修改成功");
    }

    /**
     * 用户登录+单点登录
     */
    public CommonResp login(UserLoginDTO userLoginDTO) {
        userLoginDTO.setPassword(DigestUtils.md5DigestAsHex(userLoginDTO.getPassword().getBytes()));
        User userDb = selectByLoginName(userLoginDTO.getLoginName());
        if (ObjectUtils.isEmpty(userDb)) {
            //用户名不存在
            log.info("用户名不存在, {}", userLoginDTO.getLoginName());
            return CommonResp.errorMsg(BusinessExceptionCode.LOGIN_USER_ERROR.toString());
        } else {
            if (userDb.getPassword().equals(userLoginDTO.getPassword())) {
                //登陆成功
                UserLoginResp userLoginResp = CopyUtil.copy(userDb, UserLoginResp.class);
                //生成单点登录token将token放入redis，同时将resp的返回传递到前端
                Long token = snowFlake.nextId();
                log.info("生成单点登录token:{}，放入redis中", token);
                userLoginResp.setToken(token.toString());
                //将一个类，放入reids中需要进行一个序列化
                redisTemplate.opsForValue().set(token.toString(), JSONObject.toJSONString(userLoginResp), 3600 * 24, TimeUnit.SECONDS);
                return CommonResp.ok(userLoginResp);
            } else {
                //密码不对
                log.info("密码不对, 输入密码：{}, 数据库密码：{}", userLoginDTO.getPassword(), userDb.getPassword());
                return CommonResp.errorMsg(BusinessExceptionCode.LOGIN_USER_ERROR.toString());
            }
        }

    }


}
