package com.springboot.wiki.service;

import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.mapper.EbookSnapshotMapperCust;
import com.springboot.wiki.resp.StatisticResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class EbookSnapshotService {

    @Resource
    private EbookSnapshotMapperCust ebookSnapshotMapperCust;

    /**
     * 获取首页数值数据：总阅读数、总点赞数、今日阅读数、今日点赞数、今日预计阅读数、今日预计阅读增长
     */
    public CommonResp getStatistic() {
        List<StatisticResp> statisticDataList = ebookSnapshotMapperCust.getStatistic();
        if (statisticDataList.size() < 2) {
            if (statisticDataList.isEmpty()) {
                statisticDataList.add(null);
                statisticDataList.add(null);
            } else {
                statisticDataList.add(0, statisticDataList.get(0));
            }
        }
        log.info("打印接口数据:{}", statisticDataList);
        return CommonResp.ok(statisticDataList);
    }

    /**
     * 30天数值统计
     */
    public CommonResp get30Statistic() {
        List<StatisticResp> statistic = ebookSnapshotMapperCust.get30Statistic();
        return CommonResp.ok(statistic);
    }

    public void genSnapshot() {
        ebookSnapshotMapperCust.genSnapshot();
    }

}
