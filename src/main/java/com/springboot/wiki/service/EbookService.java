package com.springboot.wiki.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.dto.EbookQueryDTO;
import com.springboot.wiki.domain.Ebook;
import com.springboot.wiki.domain.EbookExample;
import com.springboot.wiki.dto.EbookSaveDTO;
import com.springboot.wiki.mapper.EbookMapper;
import com.springboot.wiki.resp.EbookQueryResp;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.utils.CopyUtil;
import com.springboot.wiki.utils.SnowFlakeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: guohuaizhao
 * @Date: 2021/10/12/16:02
 * @Description: service服务层 创建电子服务层
 */
//使用Service注解将这个service交给Spring来管理。使spring扫描到这个类
@Service
public class EbookService {
    private static final Logger LOG = LoggerFactory.getLogger(EbookService.class);
    //将mapper写进来 声明一个私有的方法
    @Resource//将EbookMapper注入进来 也可以使用@Resource注解
    private EbookMapper ebookMapper;
    @Resource
    private SnowFlakeUtil snowFlakeUtil;

    /**
     * 查询list
     *
     * @param ebookQueryDTO
     * @return
     */
    public PageResp<EbookQueryResp> list(EbookQueryDTO ebookQueryDTO) {

        PageHelper.startPage(ebookQueryDTO.getPage(), ebookQueryDTO.getSize());
        EbookExample ebookExample = new EbookExample();
        EbookExample.Criteria criteria = ebookExample.createCriteria();

        if (!ObjectUtils.isEmpty(ebookQueryDTO.getName())) {
            criteria.andNameLike("%" + ebookQueryDTO.getName() + "%");
        }
        if (!ObjectUtils.isEmpty(ebookQueryDTO.getCategoryId2())) {
            criteria.andCategory2IdEqualTo(ebookQueryDTO.getCategoryId2());
        }

        List<Ebook> ebookList = ebookMapper.selectByExample(ebookExample);
        PageInfo<Ebook> pageInfo = new PageInfo<>(ebookList);
        LOG.info("总行数：{}", pageInfo.getTotal());
        LOG.info("总页数：{}", pageInfo.getPages());
//        List<EbookResp> respList=new ArrayList<>();
//        for (Ebook ebook : ebookList) {
//
//            EbookResp ebookResp = CopyUtil.copy(ebook, EbookResp.class);
//            respList.add(ebookResp);
//        }
        List<EbookQueryResp> list = CopyUtil.copyList(ebookList, EbookQueryResp.class);
        PageResp<EbookQueryResp> pageResp = new PageResp<>();
        pageResp.setTotal(pageInfo.getTotal());
        pageResp.setList(list);
        return pageResp;
    }

    /**
     * 功能描述:新增操作或更新
     */
    public CommonResp save(EbookSaveDTO ebookSaveDTO) {
        System.out.println("-----------------" + ebookSaveDTO);
        Ebook ebook = CopyUtil.copy(ebookSaveDTO, Ebook.class);
        if (ObjectUtils.isEmpty(ebookSaveDTO.getId())) {
            ebook.setId(snowFlakeUtil.nextId());
            ebookMapper.insert(ebook);
        } else {
            ebookMapper.updateByPrimaryKey(ebook);
        }
        return CommonResp.ok("添加成功");
    }

    /**
     * 删除电子书
     */
    public CommonResp delete(String ids) {
        System.out.println("-----------------" + ids);
        for (String id : ids.split(",")) {
            ebookMapper.deleteByPrimaryKey(Long.valueOf(id));
        }
        return CommonResp.ok("删除成功");
    }
}
