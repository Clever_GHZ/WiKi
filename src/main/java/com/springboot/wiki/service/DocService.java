package com.springboot.wiki.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.springboot.wiki.common.CommonResp;
import com.springboot.wiki.common.exception.BusinessException;
import com.springboot.wiki.common.exception.BusinessExceptionCode;
import com.springboot.wiki.domain.Content;
import com.springboot.wiki.domain.Doc;
import com.springboot.wiki.domain.Example.DocExample;
import com.springboot.wiki.dto.DocQueryDTO;
import com.springboot.wiki.dto.DocSaveDTO;
import com.springboot.wiki.mapper.ContentMapper;
import com.springboot.wiki.mapper.DocMapper;
import com.springboot.wiki.mapper.DocMapperCust;
import com.springboot.wiki.resp.PageResp;
import com.springboot.wiki.utils.CopyUtil;
import com.springboot.wiki.utils.RedisUtil;
import com.springboot.wiki.utils.RequestContext;
import com.springboot.wiki.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class DocService {
    @Resource
    private DocMapper docMapper;
    @Resource
    private SnowFlakeUtil snowFlake;
    @Resource
    private ContentMapper contentMapper;
    @Resource //
    private DocMapperCust docMapperCust;
    @Resource
    private RedisUtil redisUtil;

    @Resource
    private WsService wsService;

    /**
     * 分页查询
     *
     * @return
     */
    public CommonResp list(DocQueryDTO docQueryDTO) {
        // 1、传入页数
        PageHelper.startPage(docQueryDTO.getPage(), docQueryDTO.getSize());
        // 构造器创建条件
        DocExample docExample = new DocExample();
        docExample.setOrderByClause("sort asc");
        DocExample.Criteria criteria = docExample.createCriteria(); // 创建条件时候使用
        // 查询 所有
        List<Doc> docList = docMapper.selectByExample(docExample);
        // 创建页面信息
        PageInfo<Doc> pageInfo = new PageInfo<>(docList);
        log.info("总行数：{}", pageInfo.getTotal());
        log.info("总页数：{}", pageInfo.getPages());
        PageResp<Doc> pageResp = new PageResp();
        pageResp.setTotal(pageInfo.getTotal());
        pageResp.setList(docList);
        return CommonResp.ok(pageResp);
    }

    /**
     * 根据id查询所有
     */
    public CommonResp all(Long ebookId) {
        DocExample docExample = new DocExample();
        docExample.createCriteria().andEbookIdEqualTo(ebookId);
        docExample.setOrderByClause("sort asc");
        List<Doc> docList = docMapper.selectByExample(docExample);
        return CommonResp.ok(docList);
    }

    /**
     * 新增功能
     */
    @Transactional
    public CommonResp save(DocSaveDTO docSaveDTO) {
        log.info("docSaveDTO,{}", docSaveDTO);
        Doc doc = CopyUtil.copy(docSaveDTO, Doc.class);
        Content content = CopyUtil.copy(docSaveDTO, Content.class);
        if (ObjectUtils.isEmpty(docSaveDTO.getId())) {
            doc.setId(snowFlake.nextId());
            doc.setViewCount(0);
            doc.setVoteCount(0);
            docMapper.insert(doc);
            content.setId(doc.getId());
            contentMapper.insert(content);
        } else {
            docMapper.updateByPrimaryKey(doc);
            int count = contentMapper.updateByPrimaryKeyWithBLOBs(content);
            if (count == 0) {
                contentMapper.insert(content);
            }
        }
        return CommonResp.ok("新增成功");
    }


    /**
     * 删除功能
     */

    public CommonResp delete(String ids) {
        List<String> list = Arrays.asList(ids.split(","));
        DocExample docExample = new DocExample();
        DocExample.Criteria criteria = docExample.createCriteria();
        criteria.andIdIn(list);
        docMapper.deleteByExample(docExample);
        return CommonResp.ok("删除成功");
    }

    /**
     * 查找内容
     */
    public CommonResp findContent(Long id) {
        Content content = contentMapper.selectByPrimaryKey(id);
        docMapperCust.increaseViewCount(id);
        if (ObjectUtils.isEmpty(content)) {
            return CommonResp.errorMsg("");
        } else {
            return CommonResp.ok(content.getContent());
        }
    }

    /**
     * 点赞，+推送消息
     */
    public CommonResp vote(Long id) {
        // docMapperCust.increaseVoteCount(id);
        // 远程IP+doc.id作为key，24小时内不能重复
        String ip = RequestContext.getRemoteAddr();
        if (redisUtil.validateRepeat("DOC_VOTE_" + id + "_" + ip, 5000)) {
            docMapperCust.increaseVoteCount(id);
        } else {
            throw new BusinessException(BusinessExceptionCode.VOTE_REPEAT);
        }

        //推送消息
        Doc docDb = docMapper.selectByPrimaryKey(id);
        String logId = MDC.get("LOG_ID");
        // rocketMQTemplate.convertAndSend("VOTE_TOPIC","【"+docDb.getName()+"】被点赞");
        wsService.sendInfo("【" + docDb.getName() + "】被点赞", logId);
        return CommonResp.ok();
    }

    /**
     * 更新信息
     */
    public void updateEbookInfo() {
        docMapperCust.updateEbookInfo();
    }

}
