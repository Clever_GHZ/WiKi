package com.springboot.wiki.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: guohuaizhao
 * @Date: 2021/10/09/16:57
 * @Description:数据库名称叫什么这个实体类名就叫什么，和数据库一一对应的。
 */
public class Test {
    //数据库中存在的属性
    private Integer id;
    private String name;
    private String password;

    //生成get和set方法：快捷键alt+insert。或者右击找到Generate点击选择getter and setter弹出全选
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
