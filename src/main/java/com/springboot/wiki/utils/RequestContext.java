package com.springboot.wiki.utils;

import java.io.Serializable;

/**
 * 功能描述:获取当前远程ip的。进行线程获取，每一个线程可以获取一个IP地址
 */
public class RequestContext implements Serializable {

    private static ThreadLocal<String> remoteAddr = new ThreadLocal<>(); // 存储线程

    public static String getRemoteAddr() {
        return remoteAddr.get();
    }

    public static void setRemoteAddr(String remoteAddr) {
        RequestContext.remoteAddr.set(remoteAddr);
    }
}
