import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue';
// 全局引入css
import 'ant-design-vue/dist/antd.css';
// 全局引入图标
import * as Icons from '@ant-design/icons-vue'

import axios from "axios"; //引入axios
// 设置固定路径
axios.defaults.baseURL = process.env.VUE_APP_SERVER


const app = createApp(App);
app.use(store).use(router).use(Antd).mount('#app')

// 引入所有图标 app就是createApp返回的
const icons: any = Icons;
for (const i in icons) {
    app.component(i, icons[i]);
}

console.log('环境:',process.env.NODE_ENV)

console.log('服务端:',process.env.VUE_APP_SERVER)